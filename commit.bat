@echo OFF 

SET comentario=%1
SET branch=%2


:ValidaCommit
REM echo Validando comentario
if %comentario%=="" goto InformeComentario

REM echo Validando branch
if %branch%=="" goto InformeBranch

call git status
call git add .
call git commit -m '%comentario%'
call git push origin %branch%
call git pull origin %branch%
goto Finaliza


:InformeComentario
REM echo InformeComentario
SET /p comentario=Comentario obrigatorio: 
goto ValidaCommit

:InformeBranch
REM echo InformeBranch
SET /p branch=Nome da branch obrigatorio: 
goto ValidaCommit

:Finaliza
echo Finalizado

REM SET alias=%1
REM if "%cliente%"=="" SET /p cliente=Informe o cliente (com pelo menos 6 letras, pois sera usado para senha do .jks): 
REM SET arquivoJks=res\%cliente%\android-key\%cliente%.jks
REM if "%cliente%"=="consorcioglobo" SET alias=globo_key

REM echo ================================================================================
REM set /p choice=Cria Arquivo %cliente%.jks (criar somente se nao existir)? (Y or N)
REM if '%choice%'=='Y' goto CriaJKS
REM if '%choice%'=='y' goto CriaJKS
REM goto GeraAPK


REM :CriaJKS
REM call GeracaoJKS.bat %cliente%

REM :GeraAPK
REM echo [101;93m^+-------------------------------------------------^+[0m
REM echo [101;93m^|[0m Gerando APK nao assinado...                     [101;93m^|[0m
REM echo [101;93m^+-------------------------------------------------^+[0m
REM call cordova build --release android

REM REM pause

REM echo [102;93m^+-------------------------------------------------^+[0m
REM echo [102;93m^|[0m Processo de alinhamento do APK...               [102;93m^|[0m
REM echo [102;93m^+-------------------------------------------------^+[0m
REM call zipalign -v -p 4 platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk app-release-unsigned-aligned.apk

REM REM pause

REM echo [102;93m^+-------------------------------------------------^+[0m
REM echo [102;93m^|[0m Processo de assinatura do APK...                [102;93m^|[0m
REM echo [102;93m^+-------------------------------------------------^+[0m
REM call apksigner sign --ks-pass pass:%cliente% --ks %arquivoJks% --ks-key-alias %alias% --out %cliente%AppConsorciadoAssinado.apk app-release-unsigned-aligned.apk

REM REM pause

REM echo [103;30m^+-------------------------------------------------^+[0m
REM echo [103;30m^|[0m Fazendo verificacao da assinatura...            [103;30m^|[0m
REM echo [103;30m^+-------------------------------------------------^+[0m
REM call apksigner verify %cliente%AppConsorciadoAssinado.apk

REM REM pause

REM echo [101;93m^+-------------------------------------------------^+[0m
REM echo [101;93m^|[0m Removendo arquivos desnecessarios...            [101;93m^|[0m
REM echo [101;93m^+-------------------------------------------------^+[0m
REM del platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk
REM del app-release-unsigned-aligned.apk

REM REM pause

REM cls
REM echo [102;93m ^+ Apk %cliente% gerado com sucesso :D ^+ [0m
REM pause